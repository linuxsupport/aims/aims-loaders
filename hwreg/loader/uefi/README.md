# Instructions

We only symlink to what is already being used in `src/aims/config/uefi/` and `src/aims/loader/uefi`, so refer to them on how to upgrade the GRUB version.

**NOTE: We need to use `linuxefi` for the procurement image or some specific nodes might get stuck while loading `initrd`**
