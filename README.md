# AIMS Loaders

1. Clone this repo somewhere within an AIMS node.
1. Run `./aims2-loaders-tweak` to fill in all the LB alias references in all the config files
1. Copy the produced files back to `/aims_share/tftpboot/` where they correspond
