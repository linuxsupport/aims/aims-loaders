# How to recreate the fonts

Given that you have followed https://gitlab.cern.ch/linuxsupport/grub2/:

```bash
/tmp/rebuild_grub/bin/grub2-mkfont -s 14 -o /tmp/aims2-loaders/src/aims/config/uefi/themes/Sans-14.pf2 /usr/share/fonts/dejavu/DejaVuSansMono-Bold.ttf
/tmp/rebuild_grub/bin/grub2-mkfont -s 16 -o /tmp/aims2-loaders/src/aims/config/uefi/themes/Sans-16.pf2 /usr/share/fonts/dejavu/DejaVuSansMono-Bold.ttf
```

Test the theme and its fonts by copying to AIMS2 test and making one node point to it by setting OS as PXELINTEST on LANDB

```bash
scp /tmp/aims2-loaders/src/aims/config/uefi/themes/Sans-14.pf2 root@aimstest01:/tftpboot/aims/config/uefi/themes/
scp /tmp/aims2-loaders/src/aims/config/uefi/themes/Sans-16.pf2 root@aimstest01:/tftpboot/aims/config/uefi/themes/
```
