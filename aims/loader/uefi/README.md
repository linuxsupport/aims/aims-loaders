# Instructions

For updating the GRUB2 image you will need to build it as per <https://gitlab.cern.ch/linuxsupport/grub2> since we require a patch on the sleep command to detect more than the default ESC key.

```bash
# After having a rebuilt GRUB2 version, create an image
/tmp/rebuild_grub/bin/grub2-mkimage -c ./grub-embedded.cfg -p '(tftp)/blah' -o grubx64-with-embed.efi -O x86_64-efi efinet echo configfile net efinet tftp gzio part_gpt efi_gop efi_uga efifwsetup linux loadenv ls http gfxmenu chain png serial linux16 loopback udf sleep gfxterm png gfxterm_background minicmd terminal test

# Get the progress mod to see the download percentages
cp -v /tmp/rebuild_grub//lib/grub/x86_64-efi/progress.mod grub.cfg/x86_64-efi/

cp -v /tmp/rebuild_grub//lib/grub/x86_64-efi/{command,fs,crypto,terminal}.lst grub.cfg/x86_64-efi/
```

**NOTES: In the past we used the `linuxefi` module, which now seems to be already included in the `linux` one since already a long time. Not 100% sure as there is no doc about it but it seems `linuxefi` is used for shim, but since we are not using secure boot, no need for it, hence a universal `linux`/`initrd` for standarisation.**

## Backup project

If you cannot find the previous AFS path, there is a backup on <https://gitlab.cern.ch/linuxsupport/backup_grub2_patched_aims2loaders>

