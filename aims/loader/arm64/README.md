# Instructions

For updating the GRUB2 image you will need to build it as per <https://gitlab.cern.ch/linuxsupport/grub2> since we require a patch on the sleep command to detect more than the default ESC key.

```bash
# After having a rebuilt GRUB2 version, create an image
/tmp/rebuild_grub/bin/grub2-mkimage -c ./grub-embedded.cfg -p '(tftp)/blah' -o grubarm64-with-embed.efi -O arm64-efi efinet echo configfile net efinet tftp gzio part_gpt efi_gop efifwsetup linux loadenv ls http gfxmenu chain png serial loopback udf sleep gfxterm png gfxterm_background minicmd terminal test

# Get the progress mod to see the download percentages
cp -v /tmp/rebuild_grub/lib/grub/arm64-efi/progress.mod grub.cfg/arm64-efi/

cp -v /tmp/rebuild_grub/lib/grub/arm64-efi/{command,fs,crypto,terminal}.lst grub.cfg/arm64-efi/
```

## Backup project

If you cannot find the previous AFS path, there is a backup on <https://gitlab.cern.ch/linuxsupport/backup_grub2_patched_aims2loaders>

